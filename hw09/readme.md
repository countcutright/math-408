just a quick homework repo

the output of this program if you were to run it is:
   
    $ ruby "elliptic np extrapolation.rb"
    using   a[r] = a[1]*a[r-1] - q*a[r-2]
    
    r       a[r-2]  a[r-1]  a[r]    2^r     N[r]
    1       -       2       -1      2       4
    2       2       -1      -3      4       8
    3       -1      -3      5       8       4
    4       -3      5       1       16      16
    5       5       1       -11     32      44
    6       1       -11     9       64      56
    7       -11     9       13      128     116
    8       9       13      -31     256     288
    9       13      -31     5       512     508
    10      -31     5       57      1024    968
    11      5       57      -67     2048    2116
    12      57      -67     -47     4096    4144
    13      -67     -47     181     8192    8012
