
# equation y^2 + xy = ax^3 + bx^2 + c
a = 1
b = 0
c = 1
r = 13 # field = F[2^r]

points = ["O"]
np = 1
0.upto(2**r - 1) do |x|
	0.upto(2**r - 1) do |y|
		if (y**2 + x*y) % (2**r) == (a*(x**3) + b*(x**2) + c) % (2**r) then
			points.push([x,y])
			np = np + 1
		end
	end
end

print "points on   y^2 + xy = #{a}x^3 + #{b}x^2 + #{c}  in  F[2^#{r}]\n"
print "#{points}\n"
print "points: #{np}\n"
