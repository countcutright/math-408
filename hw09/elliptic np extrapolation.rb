a = []
n = []
# givens
q = 2
n[1] = 4
r_final = 13

# calculations
a[0] = 2
a[1] = q**1 + 1 - n[1]
print "using   a[r] = a[r-1] - q*a[r-2]\n\n"
print "r \ta[r-2] \ta[r-1] \ta[r] \t#{q}^r \tN[r]\n"
print "1 \t- \t#{a[0]} \t#{a[1]} \t2 \t#{n[1]}\n"

2.upto(r_final) do |r|
	a[r] = a[r-1] - q*a[r-2]
	n[r] = q**r + 1 - a[r]
	print "#{r} \t#{a[r-2]} \t#{a[r-1]} \t#{a[r]} \t#{q**r} \t#{n[r]}\n"
end