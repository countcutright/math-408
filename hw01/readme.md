just a quick homework repo

the output of this program if you were to run it is:

$ ruby program.rb   
primitive roots of 229:   
[6, 7, 10, 23, 24, 28, 29, 31, 35, 38, 39, 40, 41, 47, 50, 59, 63, 65, 66, 67, 69, 72, 73, 74, 77, 79, 87, 90, 92, 96, 98, 102, 105, 110, 112, 113, 116, 117, 119, 124, 127, 131, 133, 137, 139, 142, 150, 152, 155, 156, 157, 160, 162, 163, 164, 166, 170, 179, 182, 188, 189, 190, 191, 194, 198, 200, 201, 205, 206, 219, 222, 223]   
72 primitive roots   
totient(228) = 72   
all primes less than 100 for which...   
..2 is a primitive root: [3, 5, 11, 13, 19, 29, 37, 53, 59, 61, 67, 83]   
..3 is a primitive root: [5, 7, 17, 19, 29, 31, 43, 53, 79, 89]   
..4 is a primitive root: []   
   