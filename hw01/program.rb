# Aaron Cutright
# Math 408, hw 1
def is_prime(n)
	return true if n == 2
	for i in 2..n-1
		if n % i == 0
			return false
		end
	end
	return true
end
def euler_totient(n)
	y = n
	for i in 2..n+1
		if is_prime(i) and n % i == 0
			y *= 1 - 1.0/i
		end
	end
	return y.to_i
end
def find_primitive_roots(n)
	primitive_roots = []
	for i in 2..n-1
		prim = true
		y_history = []
		for exp in 1..n-1
			y = (i**exp) % n
			if y_history.index(y) or y == 0
				prim = false
				break
			end
			y_history.push(y)
		end
		primitive_roots.push(i) if prim
	end
	return primitive_roots
end
def primes_with_primitive_root_bounded(root, limit)
	primes = []
	for i in 2..limit
		if is_prime(i) and find_primitive_roots(i).index(root)
			primes.push(i)
		end
	end
	return primes
end

# part e
primitive_roots = find_primitive_roots(229)
puts "primitive roots of 229:"
puts "#{primitive_roots} \n#{primitive_roots.length} primitive roots"
puts "totient(228) = "+euler_totient(228).to_s
# part f
puts "all primes less than 100 for which..."
puts "..2 is a primitive root: " + primes_with_primitive_root_bounded(2, 100).to_s
# part g
puts "..3 is a primitive root: " + primes_with_primitive_root_bounded(3, 100).to_s
puts "..4 is a primitive root: " + primes_with_primitive_root_bounded(4, 100).to_s

# manual confirmation method for parts a-d
puts find_primitive_roots(ARGV[0].to_i).to_a