# for gram-schmidt orthonormalization
gs_vectors = [[1,3,2],[4,1,-2],[-2,1,3]]

# for guassian lattice reduction
v1 = [120670, 110521]
v2 = [323572, 296358]

# test from book => [2280,-1001] [-1324,-2376]
#v1 = [66586820, 65354729]
#v2 = [6513996, 6393464]

def dot_product(v1, v2)
	dot = 0
	for i in 0..v1.size-1 do
		dot = dot + v1[i]*v2[i]
	end
	return dot
end

def magnitude_sq(v)
	mag = 0.0 
	for i in 0..v.size-1 do
		mag = mag + v[i]**2
	end
	return mag
end

def mult_scalar(v, scalar)
	v2 = Array.new(v.size)
	for i in 0..v.size-1 do
		v2[i] = v[i] * scalar
	end
	return v2
end

def add_vectors(v1, v2)
	v = Array.new(v1.size)
	for i in 0..v1.size-1 do
		v[i] = v1[i] + v2[i]
	end
	return v
end

def subtract_vectors(v1, v2)
	v = Array.new(v1.size, 0)
	for i in 0..v1.size-1 do
		v[i] = v1[i] - v2[i]
	end
	return v
end

def gram_schmidt(vectors)
	vectors_orth = Array.new(vectors.size){Array.new(vectors[0].size)}
	mu = Array.new(vectors.size){Array.new(vectors.size)}
	vectors_orth[0] = vectors[0]

	for i in 1..vectors.size-1 do
		sum_v = Array.new(vectors_orth[0].size, 0)
		for j in 0..i-1 do
			mu[i][j] = dot_product(vectors[i], vectors_orth[j])/magnitude_sq(vectors_orth[j])
			puts "mu[#{i+1},#{j+1}] = #{mu[i][j]}"
			sum_v = add_vectors(sum_v, mult_scalar(vectors_orth[j], mu[i][j])) 
		end
		vectors_orth[i] = subtract_vectors(vectors[i],  sum_v)
		puts "v*[#{i+1}] = #{vectors_orth[i]}"
	end
	return vectors_orth
end

def gaussian_lattice_reduction(v1, v2)
	n = 0 # num of iterations
	m = 0 # multiplier
	temp = []
	puts "#{'%-5s'%'Step'}   #{'%12s'%'v1'+'%8s'%' '}   #{'%12s'%'v2'+'%8s'%' '}   #{'%5s'%'m'}"
	while(true) do
		if magnitude_sq(v2) < magnitude_sq(v1) then
			temp = v1
			v1 = v2
			v2 = temp
		end
		m = (dot_product(v1, v2)/magnitude_sq(v1)).round
		n = n + 1
		vs1 = "#{v1}"
		vs2 = "#{v2}"
		puts "#{'%-5s' % n}   #{'%20s' % vs1}   #{'%20s' % vs2}   #{'%5s' % m}"
		if m == 0 then
			return [v1,v2]
		end
		v2 = subtract_vectors(v2, mult_scalar(v1, m))
	end
end

puts "starting vectors = #{gs_vectors}"
puts "\nv* = #{gram_schmidt(gs_vectors)}"

puts "\nguassian reduction of  v1=#{v1},   v2=#{v2}"
puts "basis = #{gaussian_lattice_reduction(v1, v2)}"