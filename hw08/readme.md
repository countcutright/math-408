just a quick homework repo

the output of this program if you were to run it is:

    $ ruby elliptic.rb
    
    y^2 = x^3 + 1x + 1
    working modulo 11
    
    initialization table:
     x^2     x       y^2     points
     0       0       1       [0, 1] [0, 10]
     1       1       3       [1, 5] [1, 6]
     4       2       0       [2, 0]
     9       3       9       [3, 3] [3, 8]
     5       4       3       [4, 5] [4, 6]
     3       5       10
     3       6       3       [6, 5] [6, 6]
     5       7       10
     9       8       4       [8, 2] [8, 9] 
     4       9       2
     1       10      10
    
    Addition table:
            ||    O   || [0, 1] || [0, 10] || [1, 5] || [1, 6] || [2, 0] || [3, 3] || [3, 8] || [4, 5] || [4, 6] || [6, 5] || [6, 6] || [8, 2] || [8, 9]
        O   ||    O      [0, 1]    [0, 10]    [1, 5]    [1, 6]    [2, 0]    [3, 3]    [3, 8]    [4, 5]    [4, 6]    [6, 5]    [6, 6]    [8, 2]    [8, 9]
     [0, 1] || [0, 1]    [3, 3]       O      [4, 5]    [2, 0]    [1, 5]    [6, 6]    [0, 10]    [8, 2]    [1, 6]    [3, 8]    [6, 5]    [8, 9]    [4, 6]
     [0, 10] || [0, 10]       O      [3, 8]    [2, 0]    [4, 6]    [1, 6]    [0, 1]    [6, 5]    [1, 5]    [8, 9]    [6, 6]    [3, 3]    [4, 5]    [8, 2]
     [1, 5] || [1, 5]    [4, 5]    [2, 0]    [3, 3]       O      [0, 1]    [8, 2]    [1, 6]    [6, 6]    [0, 10]    [4, 6]    [8, 9]    [6, 5]    [3, 8]
     [1, 6] || [1, 6]    [2, 0]    [4, 6]       O      [3, 8]    [0, 10]    [1, 5]    [8, 9]    [0, 1]    [6, 5]    [8, 2]    [4, 5]    [3, 3]    [6, 6]
     [2, 0] || [2, 0]    [1, 5]    [1, 6]    [0, 1]    [0, 10]       O      [4, 5]    [4, 6]    [3, 3]    [3, 8]    [8, 9]    [8, 2]    [6, 6]    [6, 5]
     [3, 3] || [3, 3]    [6, 6]    [0, 1]    [8, 2]    [1, 5]    [4, 5]    [6, 5]       O      [8, 9]    [2, 0]    [0, 10]    [3, 8]    [4, 6]    [1, 6]
     [3, 8] || [3, 8]    [0, 10]    [6, 5]    [1, 6]    [8, 9]    [4, 6]       O      [6, 6]    [2, 0]    [8, 2]    [3, 3]    [0, 1]    [1, 5]    [4, 5]
     [4, 5] || [4, 5]    [8, 2]    [1, 5]    [6, 6]    [0, 1]    [3, 3]    [8, 9]    [2, 0]    [6, 5]       O      [1, 6]    [4, 6]    [3, 8]    [0, 10]
     [4, 6] || [4, 6]    [1, 6]    [8, 9]    [0, 10]    [6, 5]    [3, 8]    [2, 0]    [8, 2]       O      [6, 6]    [4, 5]    [1, 5]    [0, 1]    [3, 3]
     [6, 5] || [6, 5]    [3, 8]    [6, 6]    [4, 6]    [8, 2]    [8, 9]    [0, 10]    [3, 3]    [1, 6]    [4, 5]    [0, 1]       O      [2, 0]    [1, 5]
     [6, 6] || [6, 6]    [6, 5]    [3, 3]    [8, 9]    [4, 5]    [8, 2]    [3, 8]    [0, 1]    [4, 6]    [1, 5]       O      [0, 10]    [1, 6]    [2, 0]
     [8, 2] || [8, 2]    [8, 9]    [4, 5]    [6, 5]    [3, 3]    [6, 6]    [4, 6]    [1, 5]    [3, 8]    [0, 1]    [2, 0]    [1, 6]    [0, 10]       O
     [8, 9] || [8, 9]    [4, 6]    [8, 2]    [3, 8]    [6, 6]    [6, 5]    [1, 6]    [4, 5]    [0, 10]    [3, 3]    [1, 5]    [2, 0]       O      [0, 1]
    
    inital set: ["   O  ", [0, 1], [0, 10], [1, 5], [1, 6], [2, 0], [3, 3], [3, 8], [4, 5], [4, 6], [6, 5], [6, 6], [8, 2], [8, 9]]
    final set:  ["   O  ", [0, 1], [0, 10], [1, 5], [1, 6], [2, 0], [3, 3], [3, 8], [4, 5], [4, 6], [6, 5], [6, 6], [8, 2], [8, 9]]
    num of points: 14
    
