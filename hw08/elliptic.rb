# y^2 = x^3 + bx + c
$b = 1
$c = 1
$p = 11
$O = "   O  "

# brute force find the modular sqrts of n mod p
def modularSqrts(n, p)
	sqrts = []
	0.upto(p-1) do |x|
		if (x*x) % p == n then
			nx = (-x) % p
			sqrts.push(x)
			sqrts.push(nx) if nx != x
			#print "\nsqrt(#{n}) % #{p} = (#{x}, #{nx}) \n"
			return sqrts
		end
	end
	return [n] if sqrts[0].nil?
	return sqrts
end

def modularAbs(n, p)
	return n if n.nil?
	return n if n >= 0
	return (p + n) % p
end

# modular inverse using extended gcd
def modInverse(n, p)
	n = n % p
	n0 = n
	p0 = p
	x0 = 0
	x1 = 1
	return 0 if p == 1
	while n > 1 do 
		return "DNE" if p == 0
		q = n / p #quotient
		t = p
		# remainder
		p = n % p
		n = t
		t = x0
		x0 = x1 - q*x0
		x1 = t
	end
	# return positive result
	return x1 + p0 if x1 < 0
	return x1
end

def makeInitialTable(b, c, p)
	initialSet = [$O]
	squares = []
	print " x^2 \t x \t y^2 \t points \n" # label
	0.upto(p-1) do |x|
		squares.push(x**2 % p)
	end
	0.upto(p-1) do |x|
		x2 = squares[x]
		y2 = (x**3 + b*x + c) % p
		print " #{x2} \t #{x} \t #{y2}" # row
		if squares.include?(y2) then
			points = []
			y = modularSqrts(y2, p)
			p0 = initialSet.push([x, y[0]])
			print "\t #{[x, y[0]]}"
			if y[0] != y[1] and !y[1].nil? then
				p1 = initialSet.push([x, y[1]])
				print " #{[x, y[1]]}"
			end

		end
		print "\n"
	end
	return initialSet
end

def pointInSet?(point, set)
	set.each{ |s_pt| 
		return true if (s_pt[0] == point[0]) and (s_pt[1] == point[1])
	}
	return false
end

def deepCopy(arr1)
	arr2 = []
	arr1.each do |e|
		arr2.push(e)
	end
	return arr2
end

def makeAdditionTable(b, c, p, initialSet)
	finalSet = deepCopy(initialSet)
	# print top row
	initialSet.each_with_index{ |row_pt, i|
		print " " * initialSet[1].to_s.length, "  ||" if i == 0 
		print " #{row_pt}"
		print " ||" if i < initialSet.length-1
	}
	print "\n"
	# calculate all additions
	initialSet.each_with_index{ |row_pt, i|
		print " #{row_pt} ||"
		initialSet.each_with_index{ |col_pt, j|
			if i == 0 then
				print " #{col_pt}   "
			elsif j == 0 then
				print " #{row_pt}   "
			else
				#print "\n#{row_pt} + #{col_pt}"
				p3 = addPoints(b, c, p, row_pt, col_pt)
				#print " = #{p3}\n"
				print " #{p3}   "
				if !pointInSet?(p3, finalSet) then
					finalSet.push(p3)
				end
			end

		}
		print "\n"
	}
	return finalSet
end

def addPoints(b, c, p, p1, p2)
	return p2 if p1 == $O
	return p1 if p2 == $O
	x1 = p1[0]
	y1 = p1[1]
	x2 = p2[0]
	y2 = p2[1]
	#print "\nadding (#{x1},#{y1}) + (#{x2},#{y2})\n"
	slope = 0 # dummy initializer
	if x1 != x2 then
		#print "x1 != x2\n"
		div = modInverse(x2 - x1, p)
		return $0 if div == "DNE"
		slope = ((y2 - y1) * div) % p
	elsif x1 == x2 and y1 == 0 then
		#print "x1=x2, y1=0\n"
		return $O
	elsif x1 == x2 and y2 == y1 then
		#print "x1=x2, y1=y2\n"
		div = modInverse(2*y1, p)
		return $0 if div == "DNE"
		slope = ((3*(x1**2) + b) * div) % p
	elsif x1 == x2 and y2 == -y1 then
		#print "x1=x2, y1= -y2\n"
		return $O
	elsif x1 == x2 then
		#print "x1=x2\n"
		return $O
	end
	x3 = (slope**2 - x1 - x2) % p
	y3 = (-y1 + slope*(x1 - x3)) % p
	#print "\n slope:#{slope}, x3:#{x3}, y3:#{y3}\n"
	return [x3, y3]
end

print "\ny^2 = x^3 + #{$b}x + #{$c}\n"
print "working modulo #{$p} \n"
print "\ninitialization table: \n"
initialSet = makeInitialTable($b, $c, $p)
print "\nAddition table: \n"
finalSet = makeAdditionTable($b, $c, $p, initialSet)
print "\n"

print "inital set: #{initialSet}\n"
print "final set:  #{finalSet}\n"
print "num of points: #{finalSet.length}\n"